#include <unistd.h>

void	ft_swap(int *a, int *b)
{
	int c;

	c = *a;
	*a = *b;
	*b = c;
}

void	ft_rev_int_tab(int *tab, int size)
{
	int c;

	c = 0;
	while (c < (size / 2))
	{
		ft_swap(&tab[c], &tab[size - c - 1]);
		c++;
	}
}
