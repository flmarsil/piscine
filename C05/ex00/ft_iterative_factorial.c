int		ft_iterative_factorial(int nb)
{
	int result;
	int i;

	i = 1;
	if (nb < 0)
		return (0);
	result = 1;
	while (i <= nb)
	{
		result = result * i;
		i++;
	}
	return (result);
}
